var Hero = cc.Sprite.extend({
    centerToSide:29,
    centerToBottom:39,
    desiredPosition:cc.p(0, 0),
    direction:cc.p(0, 0),
    velocity:cc.p(0, 0),
    walkSpeed:80,
    actionState:'',
    ctor:function () {
        this._super("#hero_idle_00.png");

        var animFrames = loadFrames("hero_idle_", 6, ".png");
        var idleAnimation = new cc.Animation(animFrames, 1 / 12);
        this.idleAction = cc.animate(idleAnimation);

        animFrames = loadFrames("hero_walk_", 8, ".png");
        var walkAnimation = new cc.Animation(animFrames, 1 / 12);
        this.walkAction = cc.animate(walkAnimation);
    },
    update: function(dt) {
        if (this.actionState == 'kActionStateWalk') {
            this.desiredPosition = cc.pAdd(this.getPosition(), cc.pMult(this.velocity, dt));
        }
    },
    idle: function() {
        if (this.actionState !== 'kActionStateIdle') {
            this.stopAllActions();
            this.actionState = 'kActionStateIdle';
            this.runAction(this.idleAction.repeatForever());
        }
    },
    walkWithDirection: function() {
        if (this.actionState == 'kActionStateIdle') {
            this.stopAllActions();
            this.runAction(this.walkAction.repeatForever());
            this.actionState = 'kActionStateWalk';
        }
        if (this.actionState == 'kActionStateWalk') {
            this.velocity = cc.p(this.direction.x * this.walkSpeed, 
                                 this.direction.y * this.walkSpeed);
        }
        //this.setFlippedX(this.velocity.x < 0);
    }
});