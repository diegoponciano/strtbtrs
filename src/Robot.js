var Robot = cc.Sprite.extend({
    centerToBottom: 39.0,
    centerToSide: 29.0,
    mHitPoints: 100.0,
    mDamage: 10.0,
    mWalkSpeed: 80.0,
    mNextDecisionTime: 0.0,
    ctor:function () {
        this._super("#robot_idle_00.png");

        var animFrames = loadFrames("robot_idle_", 5, ".png");
        this.idleAnimation = new cc.Animation(animFrames, 1 / 12);

        animFrames = loadFrames("robot_attack_", 5, ".png");
        this.attackAnimation = new cc.Animation(animFrames, 1 / 24);

        animFrames = loadFrames("robot_walk_", 6, ".png");
        this.walkAnimation = new cc.Animation(animFrames, 1 / 12);

        animFrames = loadFrames("robot_hurt_", 3, ".png");
        this.hurtAnimation = new cc.Animation(animFrames, 1 / 12);

        animFrames = loadFrames("robot_knockout_", 5, ".png");
        this.knockedOutAnimation = new cc.Animation(animFrames, 1 / 12);
    },
    update:function (dt) {
        // Keys are only enabled on the browser
        if( cc.config.deviceType == 'browser' ) {
            var pos = this.getPosition();
            if ((MW.KEYS[cc.KEY.w] || MW.KEYS[cc.KEY.up]) && pos.y <= winSize.height) {
                pos.y += dt * this.speed;
            }
            if ((MW.KEYS[cc.KEY.s] || MW.KEYS[cc.KEY.down]) && pos.y >= 0) {
                pos.y -= dt * this.speed;
            }
            if ((MW.KEYS[cc.KEY.a] || MW.KEYS[cc.KEY.left]) && pos.x >= 0) {
                pos.x -= dt * this.speed;
            }
            if ((MW.KEYS[cc.KEY.d] || MW.KEYS[cc.KEY.right]) && pos.x <= winSize.width) {
                pos.x += dt * this.speed;
            }
            this.setPosition(pos);
        }
    },
    idle:function () {
        var animate = cc.animate(this.idleAnimation);
        this.runAction(animate.repeatForever());
    },
    attack:function () {
        this.runAction(this.attackAnimation);
    },
    walk:function () {
        this.runAction(this.walkAnimation);
    }
});