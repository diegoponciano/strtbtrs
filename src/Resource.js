var res = {
    cocos2d_html5_png : 'res/cocos2d-html5.png',
    pd_sprites_png : 'res/pd_sprites.png',
    pd_tiles_png : 'res/pd_tiles.png',
    pd_tilemap_tmx : 'res/pd_tilemap.tmx',
    pd_sprites_plist : 'res/pd_sprites.plist'
};

var g_maingame = [
    //image
    res.cocos2d_html5_png,
    res.pd_sprites_png,
    res.pd_tiles_png,

    //tmx
    res.pd_tilemap_tmx,

    //plist
    res.pd_sprites_plist,
];
