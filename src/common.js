function zeroPad(nr, base){
    var len = (String(base).length - String(nr).length)+1;
    return len > 0? new Array(len).join('0')+nr : nr;
}
function loadFrames(prefix, size, extension) {
    var animFrames = [];
    for (var i = 0; i < size; ++i) {
        var str = prefix + zeroPad(i, 10) + extension;
        var frame = cc.spriteFrameCache.getSpriteFrame(str);
        animFrames.push(frame);
    }
    return animFrames;
}
function random_range(min, max) {
    return Math.floor((Math.random() * max) + min);
}