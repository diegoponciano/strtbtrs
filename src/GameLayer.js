STATE_PLAYING = 0;
STATE_GAMEOVER = 1;
MAX_CONTAINT_WIDTH = 40;
MAX_CONTAINT_HEIGHT = 40;

var g_sharedGameLayer;

var GameLayer = cc.Layer.extend({
    _time:null,
    _ship:null,
    _backSky:null,
    _backSkyHeight:0,
    _backSkyRe:null,
    _levelManager:null,
    _tmpScore:0,
    _isBackSkyReload:false,
    _isBackTileReload:false,
    lbScore:null,
    screenRect:null,
    explosionAnimation:[],
    _beginPos:cc.p(0, 0),
    _state:STATE_PLAYING,
    _explosions:null,
    _texOpaqueBatch:null,
    _texTransparentBatch:null,
    speed:220,
    ctor:function(){
        this._super();
        this.init();
    },
    init:function () {
        this.initTileMap();
        
        winSize = cc.director.getWinSize();

        cc.spriteFrameCache.addSpriteFrames(res.pd_sprites_plist);
        this.spriteSheet = new cc.SpriteBatchNode(res.pd_sprites_png);
        this.addChild(this.spriteSheet, 5);

        this.hero = new Hero();
        this.addChild(this.hero);

        this.scheduleUpdate();

        g_sharedGameLayer = this;

        this.initRobots();
        this.initHero();

        if (cc.sys.capabilities.hasOwnProperty('keyboard'))
            cc.eventManager.addListener({
                event: cc.EventListener.KEYBOARD,
                onKeyPressed:function (key, event) {
                    MW.KEYS[key] = true;
                },
                onKeyReleased:function (key, event) {
                    MW.KEYS[key] = false;
                }
            }, this);

        if ('mouse' in cc.sys.capabilities)
            cc.eventManager.addListener({
                event: cc.EventListener.MOUSE,
                onMouseMove: function(event){
                    if(event.getButton() == cc.EventMouse.BUTTON_LEFT)
                        event.getCurrentTarget().processEvent(event);
                }
            }, this);
        if (cc.sys.capabilities.hasOwnProperty('touches')){
            cc.eventManager.addListener({
                prevTouchId: -1,
                event: cc.EventListener.TOUCH_ALL_AT_ONCE,
                onTouchesMoved:function (touches, event) {
                    var touch = touches[0];
                    if (this.prevTouchId != touch.getID())
                        this.prevTouchId = touch.getID();
                    else event.getCurrentTarget().processEvent(touches[0]);
                }
            }, this);
        }

        return true;
    },
    initRobots:function () {
        var robotCount = 50;
        this.mRobots = new Array(robotCount);

        for (var i = 0; i < robotCount; ++i) {
            var robot = new Robot();
            //mActors->addChild(robot);
            this.addChild(robot);
            //this.mRobots->addObject(robot);
            this.mRobots.push(robot);

            var minX = winSize.width + robot.centerToSide;
            var maxX = this.tileMap.getMapSize().width * this.tileMap.getTileSize().width 
                - robot.centerToSide;
            var minY = robot.centerToBottom;
            var maxY = 3 * this.tileMap.getTileSize().height + robot.centerToBottom;

            //if (rand() % 2)
            if ((new Date()).getMilliseconds() % 2)
                robot.setFlippedX(true);
            robot.setPosition(
                cc.p(random_range(minX, maxX), random_range(minY, maxY)));
            robot.mDesiredPosition = robot.getPosition();
            robot.idle();
        }
    },
    initHero:function () {
        this.hero.setPosition(cc.p(this.hero.centerToSide, 50));
        this.hero.desiredPosition = this.hero.getPosition();
        this.hero.idle();
    },
    processEvent:function (event) {
        if (this._state == STATE_PLAYING) {
            var delta = event.getDelta();
            var curPos = cc.p(this.hero.x, this.hero.y);
            curPos = cc.pAdd(curPos, delta);
            curPos = cc.pClamp(curPos, cc.p(0, 0), cc.p(winSize.width, winSize.height));
            this.hero.x = curPos.x;
	        this.hero.y = curPos.y;
	        curPos = null;
        }
    },
    update:function (dt) {
        if (this._state == STATE_PLAYING) {
            this.checkInput(dt);
            this.hero.update(dt);
            this.updatePosition(dt);
            //reorderActors();
            //updateRobots(dt);
        }
    },
    checkInput: function(dt) {
        var step = 0.1;
        var stepTimes = 12;
        if ((MW.KEYS[cc.KEY.w] || MW.KEYS[cc.KEY.up]) && this.hero.y <= winSize.height) {
            if (this.hero.direction.y <= step*stepTimes)
                this.hero.direction = cc.p(this.hero.direction.x, this.hero.direction.y+step);
        }
        else {
            if (this.hero.direction.y > 0) {
                this.hero.direction.y = 0;
            }
        }
        if ((MW.KEYS[cc.KEY.s] || MW.KEYS[cc.KEY.down]) && this.hero.y >= 0) {
            if (this.hero.direction.y >= (0-step)*stepTimes)
                this.hero.direction = cc.p(this.hero.direction.x, this.hero.direction.y-step);
        }
        else {
            if (this.hero.direction.y < 0) {
                this.hero.direction.y = 0;
            }
        }
        if ((MW.KEYS[cc.KEY.a] || MW.KEYS[cc.KEY.left]) && this.hero.x >= 0) {
            this.hero.setFlippedX(true);
            if (this.hero.direction.x >= (0-step)*stepTimes)
                this.hero.direction = cc.p(this.hero.direction.x-step, this.hero.direction.y);
        }
        else {
            if (this.hero.direction.x < 0)
                this.hero.direction.x = 0;
        }
        if ((MW.KEYS[cc.KEY.d] || MW.KEYS[cc.KEY.right]) && this.hero.x <= winSize.width) {
            this.hero.setFlippedX(false);
            if (this.hero.direction.x <= step*stepTimes)
                this.hero.direction = cc.p(this.hero.direction.x+step, this.hero.direction.y);
        }
        else {
            if (this.hero.direction.x > 0) {
                this.hero.direction.x = 0;
            }
        }
        if (this.hero.direction.x === 0 && this.hero.direction.y === 0)
            this.hero.idle();
        else
            this.hero.walkWithDirection();
    },
    updatePosition: function(dt) {
        var mapW = this.tileMap.getMapSize().width;
        var tileW = this.tileMap.getTileSize().width;
        var tileH = this.tileMap.getTileSize().height;

        var posX = Math.min(mapW * tileW - this.hero.centerToSide, Math.max(this.hero.centerToSide, this.hero.desiredPosition.x));
        var posY = Math.min(3 * tileH + this.hero.centerToBottom, Math.max(this.hero.centerToBottom, this.hero.desiredPosition.y));

        this.hero.setPosition(cc.p(posX, posY));

        this.setViewpointCenter(this.hero.getPosition());
    },
    setViewpointCenter: function(position) {
        var x = Math.max(position.x, winSize.width / 2);
        var y = Math.max(position.y, winSize.height / 2);

        x = Math.min(x, (this.tileMap.getMapSize().width * 
            this.tileMap.getTileSize().width - winSize.width / 2));
        y = Math.min(y, (this.tileMap.getMapSize().height * 
            this.tileMap.getTileSize().height - winSize.height / 2));

        var actualPosition = cc.p(x, y);
        var centerOfView = cc.p(winSize.width / 2, winSize.height / 2);
        var viewPoint = cc.pSub(centerOfView, actualPosition);

        this.setPosition(viewPoint);
    },
    initTileMap:function () {
        this.tileMap = cc.TMXTiledMap.create(res.pd_tilemap_tmx);
        this.addChild(this.tileMap, -6);
    }
});

GameLayer.scene = function () {
    var scene = new cc.Scene();
    var layer = new GameLayer();
    scene.addChild(layer, 1);
    return scene;
};
/*
GameLayer.prototype.addEnemy = function (enemy, z, tag) {
    this._texTransparentBatch.addChild(enemy, z, tag);
};

GameLayer.prototype.addExplosions = function (explosion) {
    this._explosions.addChild(explosion);
};

GameLayer.prototype.addBulletHits = function (hit, zOrder) {
    this._texOpaqueBatch.addChild(hit, zOrder);
};

GameLayer.prototype.addSpark = function (spark) {
    this._sparkBatch.addChild(spark);
};

GameLayer.prototype.addBullet = function (bullet, zOrder, mode) {
    this._texOpaqueBatch.addChild(bullet, zOrder, mode);
};*/
